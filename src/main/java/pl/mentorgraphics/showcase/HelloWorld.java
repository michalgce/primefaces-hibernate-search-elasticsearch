package pl.mentorgraphics.showcase;

import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.hibernate.search.elasticsearch.ElasticsearchQueries;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.engine.spi.QueryDescriptor;
import pl.mentorgraphics.showcase.hibernate.elasticsearch.Entry;
import pl.mentorgraphics.showcase.manager.DataFeeder;
import pl.mentorgraphics.showcase.manager.EntityManagerFactoryProvider;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "helloWorld")
public class HelloWorld implements Serializable {

    @ManagedProperty(value = "#{" + EntityManagerFactoryProvider.BEAN_NAME + "}")
    private EntityManagerFactoryProvider entityManagerFactoryProvider;

    protected String fileName = "";
    protected String ownerName = "";
    protected Double sizeFrom = 0.0;
    protected Double sizeTo = 0.0;

    protected DataFeeder df;

    public HelloWorld() {

    }

    @PostConstruct
    public void init() {
        EntityManager em = entityManagerFactoryProvider.getEntityManager();
        df = new DataFeeder(em);
    }

    @Transactional
    public List<String> findOwner(String owner) {
/*        FullTextEntityManager ftem = Search.getFullTextEntityManager(entityManagerFactoryProvider.getEntityManager());

        QueryDescriptor queryDescriptor1 =
                ElasticsearchQueries.fromQueryString("owner:" + owner + "*");

        FullTextQuery query1 =
                ftem
                        .createFullTextQuery(queryDescriptor1, Entry.class)
                        .setSort(new Sort(new SortField("id", SortField.Type.STRING)))
                        .setMaxResults(50);

        List<Entry> resultList = query1.getResultList();
        List<String> returnList = new ArrayList<String>();


        for (Entry entry : resultList) {
            returnList.add("Name: " + entry.getName() + " Owner: " + entry.getOwner());
        }

        return returnList;*/
        entityManagerFactoryProvider.getEntityManager().getTransaction().begin();
        Entry pojo = new Entry();
        pojo.setPath("ala");
        pojo.setName("ok");

        entityManagerFactoryProvider.getEntityManager().persist(pojo);
        System.out.println("ID: " + pojo.getPath());

        Entry saved = entityManagerFactoryProvider.getEntityManager().find(Entry.class, pojo.getId());
        saved.setPath("http");
        System.out.println("ID: " + saved.getPath());

        Entry saved2 = entityManagerFactoryProvider.getEntityManager().find(Entry.class, saved.getId());
        System.out.println("ID: " + saved2.getPath());
        entityManagerFactoryProvider.getEntityManager().flush();

        entityManagerFactoryProvider.getEntityManager().getTransaction().commit();

        return new ArrayList<String>();
    }

    public List<String> findFiles(String fileName) {
/*        FullTextEntityManager ftem = Search.getFullTextEntityManager(entityManagerFactoryProvider.getEntityManager());

        QueryDescriptor queryDescriptor1 =
                ElasticsearchQueries.fromQueryString("name:" + fileName + "*");

        FullTextQuery query1 =
                ftem
                        .createFullTextQuery(queryDescriptor1, Entry.class)
                        .setSort(new Sort(new SortField("id", SortField.Type.STRING)))
                        .setMaxResults(50);

        List<Entry> resultList = query1.getResultList();
        List<String> returnList = new ArrayList<String>();

        for (Entry entry : resultList) {
            returnList.add("Name: " + entry.getName() + " Owner: " + entry.getOwner());
        }

        return returnList;*/
        Entry saved2 = entityManagerFactoryProvider.getEntityManager().find(Entry.class, 1L);
        System.out.println("ID: " + saved2.getId());

        return new ArrayList<String>();
    }

    public void generateData() {
        df.generateData();
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public Double getSizeFrom() {
        return sizeFrom;
    }

    public void setSizeFrom(Double sizeFrom) {
        this.sizeFrom = sizeFrom;
    }

    public Double getSizeTo() {
        return sizeTo;
    }

    public void setSizeTo(Double sizeTo) {
        this.sizeTo = sizeTo;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setEntityManagerFactoryProvider(EntityManagerFactoryProvider entityManagerFactoryProvider) {
        this.entityManagerFactoryProvider = entityManagerFactoryProvider;
    }

    public int getProgreesFeed() {
        return df.getProgress();
    }

    public DataFeeder getDf() {
        return df;
    }
}
