package pl.mentorgraphics.showcase.manager;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class RandomNamesGenerator {

    public static final int NUMBER_OF_NAMES = 5500;
    public static final int NUMBER_OF_EXTENSIONS = 4;
    public static final int NUMBER_OF_ADDONS = 6;
    protected static final String[] NAMES = new String[NUMBER_OF_NAMES];
    protected static final String[] ADDON = { "test", "code-review", "analyze", "demo", "specification", "documentation" };
    protected static final String[] EXTENSION = { "csv", "pdf", "avi", "doc", "txt" };
    protected static final Random random = new Random();

    public RandomNamesGenerator() {
        loadFileData();
    }

    public String generateRandomFileName() {
        String fileName = "";
        int numOfFileNameParts = random.nextInt(3);
        if (numOfFileNameParts == 0) {
            numOfFileNameParts = 1;
        }

        for (int i = 0; i < numOfFileNameParts; i++) {
            fileName += getRandomName();

            int j = i + 1;
            if (j < numOfFileNameParts) {
                fileName += "-";
            }
        }
        return fileName + "_" + getRandomAddon() + "." + getRandomExtension();
    }

    protected void loadFileData() {
        Scanner scanner = null;
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("names.txt").getFile());
            scanner = new Scanner(file);

            int index = 0;

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                NAMES[index] = line;

                index++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            scanner.close();
        }
    }

    protected String getRandomName() {
        return NAMES[random.nextInt(NUMBER_OF_NAMES)];
    }

    protected String getRandomAddon() {
        return ADDON[random.nextInt(NUMBER_OF_ADDONS)];
    }

    protected String getRandomExtension() {
        return EXTENSION[random.nextInt(NUMBER_OF_EXTENSIONS)];
    }
}
