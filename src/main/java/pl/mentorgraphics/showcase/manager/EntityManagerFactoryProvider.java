package pl.mentorgraphics.showcase.manager;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.io.Serializable;

@ManagedBean(name = EntityManagerFactoryProvider.BEAN_NAME)
@SessionScoped
public class EntityManagerFactoryProvider implements Serializable {

    public static final String BEAN_NAME = "entityManagerFactoryProvider";

    protected EntityManager entityManager;

    public EntityManagerFactoryProvider() {
        entityManager = Persistence.createEntityManagerFactory("hibernate.es.pu").createEntityManager();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
