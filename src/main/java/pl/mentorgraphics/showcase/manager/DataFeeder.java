package pl.mentorgraphics.showcase.manager;

import pl.mentorgraphics.showcase.hibernate.elasticsearch.Entry;

import javax.persistence.EntityManager;
import java.util.Random;

public class DataFeeder {

    protected static final int MAX_NUMBER_OF_FILES = 9000;

    protected EntityManager em;
    protected Random random;
    protected RandomNamesGenerator namesGenerator;

    protected int progress = 0;

    public DataFeeder(EntityManager em) {
        this.em = em;
        random = new Random();
        namesGenerator = new RandomNamesGenerator();
    }

    public void generateData() {
        progress = 0;
        em.getTransaction().begin();
        generateProjects();
        em.getTransaction().commit();
    }

    protected void generateProjects() {
        final int max = 200;

        for (int i = 1; i <= max; i++) {
            String name = "Project_";
            String type = "project";
            String owner = "";
            String path = "/" + type + "_" + i;
            Entry project = Entry.create(name, type, owner, path);
            em.persist(project);

            generateBoardFolder(project);
            progress = i;
        }
    }

    protected void generateBoardFolder(Entry project) {
        String name = "Board";
        String type = "board";
        String owner = project.getName();
        String path = project.getPath() + "/" + name;

        Entry board = Entry.create(name, type, owner, path);
        em.persist(board);

        generateDocumentsFolder(board);
        generatePCBFolder(board);

        if (random.nextBoolean()) {
            generateRelatedTestFolder(board);
        }
        generateSchematicsFolder(board);
    }

    protected void generateDocumentsFolder(Entry board) {
        String name = "Documents";
        String type = "documents";
        String owner = board.getName();
        String path = board.getPath() + "/" + name;

        Entry documents = Entry.create(name, type, owner, path);
        em.persist(documents);

        generateRandomNumberOfFiles(documents);
    }

    protected void generatePCBFolder(Entry board) {
        String name = "PCB";
        String type = "pcb";
        String owner = board.getName();
        String path = board.getPath() + "/" + name;

        Entry pcb = Entry.create(name, type, owner, path);
        em.persist(pcb);

        generateRandomNumberOfFiles(pcb);
    }

    protected void generateRelatedTestFolder(Entry board) {
        String name = "RelatedTest";
        String type = "related-test";
        String owner = board.getName();
        String path = board.getPath() + "/" + name;

        Entry relatedTest = Entry.create(name, type, owner, path);
        em.persist(relatedTest);

        generateRandomNumberOfFiles(relatedTest);
    }

    protected void generateSchematicsFolder(Entry board) {
        String name = "Schematics";
        String type = "schematics";
        String owner = board.getName();
        String path = board.getPath() + "/" + name;

        Entry schematics = Entry.create(name, type, owner, path);
        em.persist(schematics);

        generateRandomNumberOfFiles(schematics);
    }

    protected void generateRandomNumberOfFiles(Entry parentFolder) {
        for (int i = 0; i < random.nextInt(MAX_NUMBER_OF_FILES); i++) {
            String name = namesGenerator.generateRandomFileName();
            String type = "file";
            String owner = parentFolder.getName();
            String path = parentFolder.getPath() + "/" + name;

            Entry documents = Entry.create(name, type, owner, path);
            em.persist(documents);
        }
    }

    public int getProgress() {
        return progress;
    }
}
