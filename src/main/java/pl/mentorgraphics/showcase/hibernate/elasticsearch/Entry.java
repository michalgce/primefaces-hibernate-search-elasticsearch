package pl.mentorgraphics.showcase.hibernate.elasticsearch;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "contents")
@Indexed(index = "projects")
public class Entry implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @Column
    @Field
    protected String name;

    @Column
    @Field
    //TODO tutaj mozna by zrobic enum'a
    protected String type;

    @Column
    @Field
    protected String owner;

    @Column
    @Field
    protected String path;

    @Column
    @Field(name = "analyzed_value")
    protected String analyzedValue;

    @Column
    @Field(name = "exact_value", analyze = Analyze.NO)
    protected String exactValue;

    public static Entry create(String name, String type, String owner, String path) {
        Entry e = new Entry();
        e.setName(name);
        e.setType(type);
        e.setOwner(owner);
        e.setPath(path);

        String value = name + " " + owner;
        e.setAnalyzedValue(value);
        e.setExactValue(value);

        return e;
    }

    public Entry() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAnalyzedValue() {
        return analyzedValue;
    }

    public void setAnalyzedValue(String analyzedValue) {
        this.analyzedValue = analyzedValue;
    }

    public String getExactValue() {
        return exactValue;
    }

    public void setExactValue(String exactValue) {
        this.exactValue = exactValue;
    }
}